#include <glib.h>
#include <libnotify/notify.h>
#include <pulse/simple.h>
#include <X11/XKBlib.h>
#include <X11/Xlib.h>

const int16_t AUDIO_BUFFER[] = {
    0, 1598, 3134, 4551, 5792, 6811, 7568, 8034, 8191, 8034, 7568, 6811,
    5792, 4551, 3134, 1598, 0, -1598, -3135, -4551, -5793, -6812, -7569,
    -8035, -8192, -8035, -7569, -6812, -5793, -4551, -3135, -1598
};

const char* NOTIFICATION_ON = "⬆";
const char* NOTIFICATION_OFF = "⇧";

void beep(int freq, int duration) {
    pa_sample_spec spec = {
        .format = PA_SAMPLE_S16NE,
        .channels = 1,
        .rate = freq * sizeof AUDIO_BUFFER / sizeof *AUDIO_BUFFER
    };

    pa_simple *simple = pa_simple_new(
            NULL, "keybeep", PA_STREAM_PLAYBACK,
            NULL, "beep", &spec, NULL, NULL, NULL);

    for(int i = 0; i < duration * freq / 1000; i++)
        pa_simple_write(simple, AUDIO_BUFFER, sizeof AUDIO_BUFFER, NULL);

    pa_simple_drain(simple, NULL);
    pa_simple_free(simple);
}

void notify(int on) {
    static NotifyNotification *notification;

    if(!notification) {
        notify_init("keybeep");
        notification = notify_notification_new(NULL, NULL, NULL);
        notify_notification_set_timeout(notification, 500);
        notify_notification_set_hint(notification, "suppress-sound", g_variant_new_boolean(TRUE));
        notify_notification_set_hint(notification, "transient", g_variant_new_boolean(TRUE));
    }

    notify_notification_update(notification, on ? NOTIFICATION_ON : NOTIFICATION_OFF, NULL, NULL);
    notify_notification_show(notification, NULL);
}

void changed(int on) {
    notify(on);
    beep(on ? 1567 : 1174, 50);
}

int main(void) {
    int xkbEvent;
    Display *d = XkbOpenDisplay(NULL, &xkbEvent, NULL, NULL, NULL, NULL);
    if(!d)
        return 1;

    XkbSelectEvents(d, XkbUseCoreKbd, XkbIndicatorStateNotifyMask, XkbIndicatorStateNotifyMask);

    for(;;) {
        XkbEvent e;

        XNextEvent(d, (XEvent*)&e);
        if(xkbEvent != e.type || XkbIndicatorStateNotify != e.any.xkb_type)
            continue;

        changed(e.indicators.changed & e.indicators.state);
    }

    XCloseDisplay(d);

    return 0;
}
